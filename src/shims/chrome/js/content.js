var $ = function(selector) {
    return document.querySelector(selector);
};
var media = {
    title: "",
    tabId: -1,
    width: 0,
    height: 0,
    domain: "",
    src: []
};
var manycam = {};
manycam.$content = {
    getDomain: function(url) {
        if (url === null)
            return url;
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match !== null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            url = match[2];
        }
        subs = url.split(">");
        if (subs.length > 2) {
            return subs[1] + "." + subs[2];
        } else {
            return url;
        }
    },
    buttonClick: function(e) {
        window.open("http://manycams.com");
    },
    buttonDisplay: function(e) {
        e.preventDefault();
        video = e.target;
        id = video.id;
        if ($('#manycam_' + id)) {
            $('#manycam_' + id).style.display = "block";
        }
    },
    buttonHide: function(e) {
        e.preventDefault();
        video = e.target;
        id = video.id;
        if ($('#manycam_' + id)) {
            $('#manycam_' + id).style.display = "none";
        }
    },
    createManycamButton: function(video) {
        id = video.id;
        if (!$('#manycam_' + id)) {
            var div = document.createElement("div");
            div.setAttribute("role", "toolbar");
            div.setAttribute("style", "z-index:10000;width:30px;height:30px;position: absolute; left: 10px; top : 10px; text-align:center");
            var button = document.createElement("button");
            button.setAttribute("id", "manycam_" + id);
            button.onclick = manycam.$content.buttonClick;
            button.style = "opacity: 1;background-color:transparent;border:0px;cursor:pointer;";
            img = document.createElement("img");
            img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAIy0lEQVRYw82YW4ydVRXHf2vv/X3nO2eurZ22tLQVBFsFRPQBjBovEI1gxEh88PoiPJD4ZqImmvig8fLEs08mBCVRYowhEBVBRIggCBWUtirtlF7oTKfTmc45323vtXw4Z8qUthSBGHeyc5Kdk+/7fWuv9V8X+D9b8mY8xPbucsB6YBqYAiaBHtABcsCN/hqBWWC37NxTnetZ4XW8vAAuBi4H3gVcBmwfwRSj3QEywI/26ocrcAL4OvDrN2Qh27urA9wGfBPYMHrp6133AZ+UnXvsdVvI8DvB3W64rUJCiG/klq8YWbL8r4D+dfCYF2GrIDcespduCDq73UwxrXDpOIXuZkyeI2MRwf4boC3ADHDwNV3ZC4fmJvLgP+Sc+4p37voQ/IQImEFSJSYlxkTVtLTNIt34ODNyD105hKd6rVA3yc49972qhfYdOCpZ8Jdkwd/W6WS3F3k2FbxHnGBmqBpRlZASMTicdzRhA3XzMQ6Uu5i2+5l0D1LSUJFTW6DFozgEI6BkRAppaOld8aVnn3/YhpHX3HWV2FkW+tfBYzs6Wfhht8g/Ndbt9LLgERGMIUxKSlQlJaWNibJuUFVUjappaaol9tX7uaNtKfEkc6TRZQrgMBxGkMSk335gW/7uPQaLwL+BnwPPnQba88KRjUUnv2e81/lgr5uTBY8ThwjoyDp107K0dIp+v8TMSNFo24iqgne4LNAm4xeDkh/HxNKr3NeOMMUHutuQl23yFPCFAPDsvoO5c/LVTh7en2dhaBUDw4a/ZlRVw8KJRZw4NqyfRrzn4MmatmqZCqCppaprTBM35Dn7tOZe1fPGYkRJZgQ5DXQZcFEAcM69LwT/uSzzDmHkL4rJkL+JkfmFRYo8Jyu6PDTX8tMXBrxwKmJqXFzArW8NXDE+hqtKenXFJ4Lnr42eHUZrgPTMyCyAjf6ZPbNTmfdfKzr59XkexImAyUh7QA3m5xfxztHpjXHH8yWPLiSuu6iLmvDQsYZ9feP+Y4lowjXrc4ImJlPkJMJuO7cc9CRjRzZJEL965IBHHHAxIu9zTtyq46aR48aUaGNkpT9gcmyM3x1tOd4Kt2wfY2c345rxnM6KgwTLEe58MfKHBaVXFASED4qcV+gMe6VyOWC9Ay4V4RJjpDFqQ50ZQS0t9+kWBZUJvz3a8NFNXboqEOHQXKI+BpwQEFgUx52HFXOBzHu2YczUh5B2DnQAll4BddaaCGq206C3ah0zw0RQHUbYicUltmzcwPPLiX4LPROiwuyJyF1PDUgJWBLY5GDS83SjHKxggwiFCNtXnuRotR8RD34CwjRkG2DsSii2gmRrgbKgauvNTJIqAhiCiiAYIlDXLcE7lurInvmWe0+VlNF49EDD8b4OBSZ3EDxMeLRWDpXKTEdQVaZ8d2gNSxBPDnd1gKp9keXuRnqdXWcq9arGaDKSKJoEJ4KMwlHNOLVS4VqjXyt3/mOAGTTJXoaZ8DARYNyDRjxDsTTT8+pQxRLL6QibORPIpZReamPSqIkYlRiVdrRjUjpFh4UTy2zPja2TUDt7GabjYCrA+jBUkDHPWJ14+7jQxEgyYyGtDN3VCwSBXJCuQ4tEtOYsX3eq9u+YdBBXo0uVpImUhrvb67K80qfnjU9u8fi32LD0yhxMBljnYauHCQdl4iOFMeUTddvQEDmcLUHXQU9gTKAnWIdRjJ1lwcols/0xpf0ppdPhvhr6MSkiDvPCyqDkxk2ej1/qcNuA7R62ObjUwxYPjXHp8Zovz0BVllRtw2FbYt73X64dL1wOLocU06zAb5o27RKRTE77jw3zjBgTk+MsLC6xyXu+946cXZOJ+1aUY2OelCu9k5H3DBpu2+J4W94we/Q4vptxf/N3Euf2IxFB8GuPEjAXVHUQE79q2niz93K5c24EIqeLrizLKHpdji6cYGZ6ilu3dflYBXtXIo0aG8aEqzc7tOxzaO4EeZGxl3meTLPnNYXD488M+QZYCNdfd6U98Phzf2lj/Klv3XeyDHHiwOy0hQ3o9XqIcxw+fpzceWamp9ixvsBMKauGuSOLtBrpFBmlTzxQ7+WoLV8AKF97NADmA8AN117ZPPDnZ3/UCO/Esk9lmS9E3CuU1CiKnE2bN7C8fIqDLx2hrlvEObLgGR/vMZ51aUj8sn6G38TniZw/7L0LlknRADWwMupC/nk61dxw3VXVQ0/8/RutxNKwz2bB95xzZ3+Zc0xPT9HrdRn0+8SUcM4RvKeSlifC3/iT/wvrixYzOQNJABFwYmRSniTM3Q3sBg4DT951lcydkfuS2iwpfVvVZs3s68H7wnt3WiTXrjzPCSHQti1t23LSlnmkeIKnO88yI+VInc/lzKtwcT5yz0+obnxqtXw9q6a+aOO0F5FxJ+JS0qasm6JphiWWuKGCM6r2zYwYE3UT6Q9qjvb7xClPZ12XQVG+hq7PFqA+vBbmLCBBrnUi33XOvT/PQt4rcqq6ZXmlpCwb6iaiNkwJMSl1E6nrSIyJjnR479I1XO7eysP5w7zojlxIc44xrKfP3XXsOXAkE/i+9+4D3jnn/TD8x7qOEDxl1VDWLf2yomkSzqDbyRnvFXjv6OQB7z0hbKarjrvdr1ih/2pAe0cOfW6gk4unJtatm7g68965EYyZoSNHCMHTFchzj+qrN4VviesYp8dK6J+/EYY/PnbZz87fSi+f6tcIi+umxqd63Q7iZNht2LBOWm13zM4Po6pUZcVcf24wmCnr0eQjjBLH6mqBJ4EnLjT9GPT71Q/aNn2r6GTbet1CssxjQNREGsGs5TEzTI0YI/2VFfr9PlVVHyul/Fa9ud2/ZhyzNpPVwN7HLvvZ8QtOP37/6G5xzm0TkU+b2c2qugmYRGwcs0zVnJqKarIUk8Y2Nm3bDmKMy6rpgJk9APzii5+96dCbPrB68LG/dc3sElW9WDVtVtXJlFKuqqIpmao2KaWTqjqvqofNdN/nb/lE8z+boN334ONOVUVV0ZRQVfvMTR/WN3uk9x/v59Ds3RpCrwAAAABJRU5ErkJggg==';
            img.style = 'width:28px;height:28px;padding-top:1px';
            button.appendChild(img);
            div.appendChild(button);
            var player = document.querySelector("#player");
            if (!player) {
                player = video.parentElement;
            }
            player.appendChild(div);
        }
    },
    onMessage: function(event) {
        //console.log("onMessageFromDOM:" + JSON.stringify(event));
        videos = document.querySelectorAll("video");
        console.log("videos:" + videos.length);
        for (var i = 0; i < videos.length; i++) {
            video = videos[i];
            if (!video.src || video.src === "")
                continue;
            video.parentElement.onmouseover = manycam.$content.buttonDisplay;
            video.parentElement.onmouseout = manycam.$content.buttonHide;
            media.src.push(video.src);
            if (video.getAttribute("data-video-width"))
                media.width = video.getAttribute("data-video-width");
            if (video.getAttribute("data-video-height"))
                media.height = video.getAttribute("data-video-height");
            if (video.getAttribute("title"))
                media.title = video.getAttribute("title");
            if (video.getAttribute("operadetachedviewtitle"))
                media.title = video.getAttribute("operadetachedviewtitle");
            if (media.src.length > 0) {
                manycam.$content.createManycamButton(video);
            }
        }
    },
    $init: function() {
        media.domain = manycam.$content.getDomain(document.location.href);
        window.addEventListener('message', manycam.$content.onMessage);
    }
};
document.addEventListener('DOMContentLoaded', manycam.$content.$init);
window.onresize = manycam.$content.onMessage;