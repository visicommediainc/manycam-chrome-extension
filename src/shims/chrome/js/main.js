manycam.$main = {
    onClicked: function(tab) {
        chrome.tabs.create({ url: "http://manycam.com" });
    },
    onMessage: function(message, sender, sendResponse) {
        switch (message.type) {
            case "getTabId":
                {
                    sendResponse({ tabId: sender.tab.id });
                }
                break;
        }
    },
    onBeforeRequest: function(request) {
        var url = request.url;
        if (!manycam.$utils.checkSupportedFormat(url)) {
            return { cancel: false };
        }
        console.log("url:" + request.url);
    },
    init: function() {
        console.log("manycam init");
    }
};


document.addEventListener("DOMContentLoaded", function() {
    manycam.$main.init();
    chrome.browserAction.onClicked.addListener(manycam.$main.onClicked);
    chrome.extension.onMessage.addListener(manycam.$main.onMessage);
    chrome.webRequest.onBeforeRequest.addListener(manycam.$main.onBeforeRequest, { urls: ["<all_urls>"] }, ["blocking"]);
});